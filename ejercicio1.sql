create table provincias(
    PRO_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRO_DESCRIPCION VARCHAR(45) NOT NULL,
    PRIMARY KEY(PRO_ID)
); 
 

create table partidos(
    PAR_ID int unsigned not null auto_increment,
    PRO_ID int unsigned not null,
    PAR_DESCRIPCION varchar(45) not null, 
    primary key (PAR_ID),
    CONSTRAINT FK_partidos_provincia FOREIGN KEY FK_partidos_provincia (PRO_ID)
        references provincias (PRO_ID)
        on delete restrict
        on update restrict
); 

insert into provincias(PRO_DESCRIPCION) values ('Buenos aires');
insert into provincias(PRO_DESCRIPCION) values ('san luis');
insert into provincias(PRO_DESCRIPCION) values ('Tierra del fuego');
insert into provincias(PRO_DESCRIPCION) values ('Mendoza');


insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'La Matanza');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'moron');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'San Justo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'moreno');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Belgrano');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Chacabuco');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Junin');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'San Martin');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Ushuaia');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Rio Grande');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Antartida Argentina');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Tolhuin');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Junin');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'La paz');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'San martin');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Santa Rosa');


describe partidos;
DESCRIBE provincias; 

select * from provincias 
select * from partidos

delete from provincias 
where PRO_ID=1

delete from provincias 
where PRO_ID=5

delete from partidos
where PRO_ID=4
delete from provincias
where PRO_ID=4

update provincias set PRO_DESCRIPCION='bsas' where PRO_ID=1

update provincias set PRO_DESCRIPCION='buenos mates' where PRO_ID=3

update partidos set PAR_DESCRIPCION='el mejor' where PAR_ID=1

update provincias set PRO_DESCRIPCION='cba' where PRO_ID=2

